from flask import Flask, request, redirect, url_for, render_template
import cv2, os, logging, shutil
from flask.ext.mysql import MySQL
import numpy as np
from PIL import Image
import json
import glob
from uuid import uuid4

app = Flask(__name__)
mysql = MySQL()

cascadePath = 'FaceDetection/static/cascades/facesCascade.xml'

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'root'
app.config['MYSQL_DATABASE_DB'] = 'detect'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

class users(object):
    firstName = ""
    lastName = ""
    
    def __init__(self, firstName, lastName):
        self.firstName = firstName
        self.lastName = lastName
       
@app.route("/")
def index():
    return render_template("index.html")

@app.route("/about")
def about():
    return render_template("index.html")

@app.route("/profile")
def profile():
    return render_template("profile.html")


@app.route("/upload", methods=['POST'])
def upload():
    _firstName = request.form['firstName']
    _lastName = request.form['lastName']

    searchUsers = "select id from userDetails where firstName = '{}' and lastName = '{}'".format(_firstName, _lastName)
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute(searchUsers)
    fetchLabel = cursor.fetchone()
    _label = None
    if fetchLabel is None:
        insertUser = "insert into userDetails (firstName, lastName) values ('{}','{}')".format(_firstName, _lastName)
        cursor2 = conn.cursor()
        cursor2.execute(insertUser)
        conn.commit()
        cursor.execute(searchUsers)
        result = cursor.fetchone()
        _label = result[0]
    else:
        _label = fetchLabel[0]

    form = request.form

    upload_key = str(_label)

    is_ajax = False
    if form.get("__ajax", None) == "true":
        is_ajax = True

    target = "FaceDetection/static/uploads/{}".format(upload_key)
    try:
        if not os.path.exists(target):
            os.mkdir(target)
    except:
        if is_ajax:
            return ajax_response(False, "Error creating directory".format(target))
        else:
            return "Error creating directory".format(target)


    for upload in request.files.getlist("file"):
        filename = upload.filename.rsplit("/")[0]
        destination = "/".join([target, filename])
        upload.save(destination)

    if is_ajax:
        return ajax_response(True, upload_key)
    else:
        return redirect(url_for("profile"))

@app.route("/training")
def training():
    root = "FaceDetection/static/uploads"
    count = len([f for f in os.listdir(root) if os.path.isdir(os.path.join(root, f))])

    return render_template("training.html",
        count = count
    )

@app.route("/train", methods=['POST'])
def train():

    cascadeFilePath = "FaceDetection/static/cascades/haarcascade_frontalface_default.xml"
    faceCascade = cv2.CascadeClassifier(cascadeFilePath)
    recognizer = cv2.face.createLBPHFaceRecognizer()
    if os.path.isfile(cascadePath):
        recognizer.load(cascadePath)

    imageRoot = "FaceDetection/static/uploads"
    count = len([f for f in os.listdir(imageRoot) if os.path.isdir(os.path.join(imageRoot, f))])
    labels = []
    images = []
    if count > 0:
        for label in os.listdir(imageRoot):
            folder = os.path.join(imageRoot, label)
            if os.path.isdir(folder):
                for imageFile in os.listdir(folder):
                    imagePath = os.path.join(folder, imageFile)
                    image_pil = Image.open(imagePath).convert('L')
                    image = np.array(image_pil, 'uint8')
                    nbr = int(label)
                    faces = faceCascade.detectMultiScale(image)
                    for (x, y, w, h) in faces:
                        images.append(image[y: y + h, x: x + w])
                        labels.append(nbr)
                        cv2.waitKey(50)

        recognizer.update(images, np.array(labels))
        recognizer.train(images, np.array(labels))
        recognizer.save(cascadePath)

        for label in os.listdir(imageRoot):
            folder = os.path.join(imageRoot, label)
            if os.path.isdir(folder):
                shutil.rmtree(folder)

    return render_template("recognize.html")

@app.route("/recognition")
def recognition():
    return render_template("recognize.html")

@app.route("/recognize", methods=['POST'])
def recognize():
    file = request.files['file']
    cascadeFilePath = "FaceDetection/static/cascades/haarcascade_frontalface_default.xml"
    recognizePath = 'FaceDetection/static/recognize'
    recognizer = cv2.face.createLBPHFaceRecognizer()
    if os.path.isfile(cascadePath):
        recognizer.load(cascadePath)
    file.save(os.path.join(recognizePath, file.filename))
    predict_image_pil = Image.open(os.path.join(recognizePath, file.filename)).convert('L')
    predict_image = np.array(predict_image_pil, 'uint8')
    faceCascade = cv2.CascadeClassifier(cascadeFilePath)
    faces = faceCascade.detectMultiScale(predict_image)
    predicted_labels = []
    for (x, y, w, h) in faces:
        nbr_predicted = recognizer.predict(predict_image[y: y + h, x: x + w])
        predicted_labels.append(nbr_predicted)
    os.remove(os.path.join(recognizePath, file.filename))
    return recognition(predicted_labels)

def recognition(predicted_labels):
    people = []
    for i in range(len(predicted_labels)):
        searchUsers = "select firstName, lastName from userDetails where id = {}".format(predicted_labels[i])
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute(searchUsers)
        result = cursor.fetchone()
        detail = users(result[0], result[1])
        people.append(detail)
    return render_template("result.html",
        peopleList = people
    )

def ajax_response(status, msg):
    status_code = "ok" if status else "error"
    return json.dumps(dict(
        status=status_code,
        msg=msg,
    ))